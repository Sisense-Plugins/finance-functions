#	Financial Functions

This plugin was created to enable additional financial calculations within a pivot table.  The initial version of this plugin supports IRR, XIRR, & NPV functions that match what you would expect in Excel/Google Sheets.  This plugin works by making additional queries against Elasticube to gather data required by these functions, and calculates a new result to insert into the table.

![alt Dashboard](screenshots/dashboard.png)
An important note is that this implementation is just an example of how to create additional formulas.  All business logic for deciding what functions are supported is contained in _finance.js_ and _functions.js__.  

__Step 1: Copy the plugin__ - Copy the financejs folder into your C:\program files\sisense\PrismWeb\plugins directory.

__Step 2: Create the pivot__ - The main challenge with functions like these is that they require more data, than just what's displayed in the pivot table.  In order to specify the extra dimensions needed, you can create a multi-pass aggregation function, that this plugin will use to generate a new query.  The screenshots below show how to create a formula that includes the time dimension needed to get supporting data used by the functions.

Formula for XIRR or IRR: Requires a date dimension (month end) as well as an aggregation (sum of payments)

![alt XIRR Formula](screenshots/formula-xirr.png)

Formula for NPV: Requires a date dimension (month end), a discount rate ( 0.1 for 10% ), as well as an aggregation (sum of payments)

![alt XIRR Formula](screenshots/formula-npv.png)

Now that the formula includes where to find the data, click on the settings menu for the value and select the Finance function you want to apply.

![alt Widget Editor view](screenshots/widget-editor.png)

Same formulas being used in Google Sheets

![alt Google Sheets Verification](screenshots/google-sheets.png)


__Notes/Reference__

* This sample has been confirmed working on Sisense version 7.1, and should be backwards compatible with previous version
* This plugin supports only pivot tables with rows, not dimensions as columns
* This plugin does not support exporting to PDF, XLS, or CSV
* This plugin uses code found at http://financejs.org/ and https://gist.github.com/ghalimi/4669712 in order to calculate the financial functions.