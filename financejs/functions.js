
//	Instantiate the finance.js object
var finance = new Finance();

//	Function to fill in gaps within a resultset
function cleanResult(column, numRows){

	//	Loop through each row in the column
	for (var i=0; i< numRows; i++){

		//	Check the data type
		if (typeof column[i] !== "number") {

			//	Not a valid data type, so assign a zero
			column[i] = 0;
		}
	}

	//	Returned the clean data
	return column;
}

//	Function to auto-fill any dates with no data (for cash flows)
function dataPadder(result) {

	//	Define the value to pad with
	var padValue = 0;

	//	Figure out the min and max date in the data set
	var dates = {
		min: $$get(result.data, '0.date', null),
		max: $$get(result.data, (result.data.length - 1) + '.date', null)
	}

	//	Figure out how many days are in between the range
	var numDays = dates.max.diff(dates.min, 'days') + 1;

	//	Init an array of clean data points
	var cleanData = [];

	//	Loop through every day
	for (var i=0; i< numDays; i++){

		//	Calculate the date for this point in the loop
		var today = moment(dates.min._d).add(i,'days');

		//	Loop for a datapoint for a matching date
		var match = result.data.filter(function(row){
			return today.isSame(row.date);
		})

		//	Take the value if it's there, otherwise insert a zero
		var value = match.length > 0 ? $$get(match, '0.value', padValue) : padValue;

		//	Save to the array
		cleanData.push(value);
	}

	//	Pass back the data object
	return cleanData;
}

//	Function to determine the unique key per row
function getKey(queryStats, row){

	//	Init the key string
	var key = '';

	//	Loop through each dimension, and append to the key
	for (var j=0; j<queryStats.dimensions.length; j++){
		key += ('-' + $$get(row, j + '.text', ''));
	}

	return key;
}

//	Define the list of finance.js functions that have hooks to Sisense
var myFunctions = [
	{
		label:'IRR',
		tooltip: 'Internal Rate of Return: requires an initial investment + cash flow array',
		run: function(data, queryStats) {

			//	Create a dictionary of the results, grouped by dimension member
			var newValues = {},
				rowNumber = 0,
				dateColumn = queryStats.dimensions.length,
				valueColumn = dateColumn + 1;

			//	Loop through every row of data
			data.values.forEach(function(row){

				//	Figure out the unique identified for this row
				//var key = $$get(row, '0.text', null);
				var key = getKey(queryStats, row);
				if (key){

					//	Create an entry, if it doesn't exist
					if (!newValues[key]) {
						newValues[key] = {
							index: rowNumber,
							data: [],
							values: []
						}
						rowNumber += 1;
					}

					//	Save the data points
					newValues[key].data.push({
						"date": moment(row[dateColumn].data),
						"value": row[valueColumn].data
					});
					newValues[key].values.push(row[valueColumn].data);
				}
			})

			//	Loop through each row in the table, and calculate the IRR
			var column = [];
			for (key in newValues){

				//	Double check the row exists
				if (newValues.hasOwnProperty(key)){

					//	Get a single row
					var row = newValues[key];

					//	Make sure the data contains values for every day
					//var cleanData = dataPadder(row);

					//	Calculate the IRR
					try {
						//column[row.index] = finance.IRR.apply(window, cleanData) / 100;
						column[row.index] = finance.IRR.apply(window, row.values) / 100;
					} catch(err){
						console.log('IRR Function Error:' + err.message)
						return cleanResult([], rowNumber);
					}
				}
			}

			//	Return the new column of IRR values
			return cleanResult(column, rowNumber);
		}
	},{
		label:'XIRR',
		tooltip: 'XIRR: requires an initial investment + cash flow array (by day)',
		run: function(data, queryStats) {

			//	Create a dictionary of the results, grouped by dimension member
			var newValues = {},
				rowNumber = 0,
				dateColumn = queryStats.dimensions.length,
				valueColumn = dateColumn + 1;

			//	Loop through every row of data
			data.values.forEach(function(row){

				//	Figure out the unique identified for this row
				var key = getKey(queryStats, row);
				if (key){

					//	Create an entry, if it doesn't exist
					if (!newValues[key]) {
						newValues[key] = {
							index: rowNumber,
							data: [],
							dates: [],
							values: []
						}
						rowNumber += 1;
					}

					//	Save the data points
					newValues[key].data.push({
						"date": moment(row[dateColumn].data),
						"value": row[valueColumn].data
					});
					newValues[key].values.push(row[valueColumn].data);
					newValues[key].dates.push(moment(row[dateColumn].data)._d);
				}
			})

			//	Loop through each row in the table, and calculate the IRR
			var column = [];
			for (key in newValues){

				//	Double check the row exists
				if (newValues.hasOwnProperty(key)){

					//	Get a single row
					var row = newValues[key];

					//	Calculate the XIRR
					try {
						//column[row.index] = window.xirr(row.values, row.dates);
						column[row.index] = finance.XIRR(row.values, row.dates);
					} catch(err){
						console.log('XIRR Function Error:' + err.message)
						return cleanResult([], rowNumber);
					}
				}
			}

			//	Return the new column of IRR values
			return cleanResult(column, rowNumber);
		}
	},{
		label:'NPV',
		tooltip: 'Net Present Value: requires a discount rate & initial investment + cash flow array',
		run: function(data, queryStats) {

			//	Create a dictionary of the results, grouped by dimension member
			var newValues = {},
				rowNumber = 0,
				dateColumn = queryStats.dimensions.length,
				rateColumn = dateColumn + 1,
				valueColumn = dateColumn + 2;

			//	Loop through every row of data
			data.values.forEach(function(row){

				//	Figure out the unique identified for this row
				var key = getKey(queryStats, row);
				if (key){

					//	Create an entry, if it doesn't exist
					if (!newValues[key]) {

						//	Figure out the rate (use 0 if not found)
						var rate = $$get(row, rateColumn + '.data',0) * 100;

						newValues[key] = {
							index: rowNumber,
							rate: rate,
							data: [],
							values: [rate]
						}
						rowNumber += 1;
					}

					//	Save the data points
					newValues[key].data.push({
						"date": moment(row[dateColumn].data),
						"value": row[valueColumn].data
					});
					newValues[key].values.push(row[valueColumn].data);
				}
			})

			//	Loop through each row in the table, and calculate the IRR
			var column = [];
			for (key in newValues){

				//	Double check the row exists
				if (newValues.hasOwnProperty(key)){

					//	Get a single row
					var row = newValues[key];

					//	Calculate the NPV
					try {
						
						column[row.index] = finance.NPV.apply(window, row.values);
					} catch(err){
						console.log('NPV Function Error:' + err.message)
						return cleanResult([], rowNumber);
					}
				}
			}

			//	Return the new column of IRR values
			return cleanResult(column, rowNumber);
		}
	}
]