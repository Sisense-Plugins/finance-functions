//	Initialize the copyWidget object
prism.run([function() {

	//	Define the menu label
	var headerMenuCaption = "Finance",
		propName = 'financeFunctions',
		hidePropName = 'hideValue';

	//	What chart types are supported?
	var supportedChartTypes = ["pivot"];

	//	default settings object	
	var defaultSettings = {};

	//////////////////////////////////
	//	Utility Functions			//
	//////////////////////////////////

	//	Function to determine if this chart type is supported
	function widgetIsSupported(type){
		if (supportedChartTypes.indexOf(type) >= 0) {
			return true;
		} else {
			return false;
		}
	}

	//  Function to make sure this is a settings menu for a measure
    function isSettingsMenu(args,location) {
        
        //  Try to evaluate
        try {

        	//	Must be in the widget editor
			var widgetEditorOpen = (prism.$ngscope.appstate === "widget");
			if (!widgetEditorOpen){
				return false
			}

            // Widget must be an allowed chart            
            if(!widgetIsSupported(prism.$ngscope.widget.type)){
                return false;
            }

            if (location == "metadataMenu") {

	            //	Make sure the user clicked on a metadata item menu
	            var isMetadataItemMenu = (args.settings.name === "widget-metadataitem");
	            if (!isMetadataItemMenu){
	            	return false;
	            }

	        } else if (location == "widgetMenu"){

	        	//	Make sure the user clicked on a widget menu
	            var isWidgetMenu = (args.ui.css == "w-menu-host");
	            if (!isWidgetMenu){
	            	return false;
	            }
	        }

        } catch(e) {
            return false;
        }

        return true;
    }

    //	Function to add an event handler
    function addHandler(widget,event,handler){
        if (!hasEventHandler(widget,event,handler)){
            widget.on(event, handler);
        }
    }

    //  Function to check if model contains certain event handler for specified event name
    function hasEventHandler(model, eventName, handler) {

        return model.$$eventHandlers(eventName).indexOf(handler) >= 0;
    }

    //	Event handler for when the menu item was clicked
    function menuItemClicked(){

    	//	Get a reference to the widget's settings
    	var settings = this.widget.options[propName];

    	//	Check for any existing settings for this function
    	var funcSettings = $$get(settings, this.functionObj.label, null);
    	if (funcSettings) {
    		//	Existing settings found for this financial function
    		var existingSetting = $$get(funcSettings, this.caption, false);

    		//	Init/Flip the existing setting
    		settings[this.functionObj.label][this.caption] = !existingSetting;

    	} else {
    		//	Init brand new settings (all financial functions)
    		settings = {};
    		settings[this.functionObj.label] = {};
    		settings[this.functionObj.label][this.caption] = true;
    	}

    	//	Save settings back to widget
    	this.widget.options[propName] = settings;

		//	Redraw the widget
		this.widget.refresh();
	}

	//	Event handler for when the metadata item menu was clicked
	function metadataItemClicked(){

    	//	Get a reference to the widget's settings
    	var currentSelection = this.item.jaql[propName],
    		caption = this.caption;

    	//	was this property selected before?
    	var existing = (currentSelection == caption);

    	//	Update the settings of the item
    	if (existing){
    		delete this.item.jaql[propName]
    	} else {
    		this.item.jaql[propName] = caption;
    	}

		//	Redraw the widget
		this.widget.refresh();
	}

    //	Function to add the widget menu item
    function addMenuItem(event, args){

		//	Check for metadata menu
		var metadataMenu = isSettingsMenu(args,'metadataMenu');
		if (metadataMenu) {
			addMetadataMenuItem(event, args);
			return;
		}
    }

    //	Function to add the metadata menu item
    function addMetadataMenuItem(event,args){

		//	Get the widget object
		var widget = event.currentScope.widget;

		//	Get the metadata item that was clicked
		var item = args.settings.item,
			panelItems = args.settings.item.$$panel.items;

		//  Create a separator
        var separator = {
            type: "separator"
        };

		//	Create settings menu
		var options = {
			caption: headerMenuCaption,
			items:[]
		};

		//	Loop through each available function
		myFunctions.forEach(function(func){

			//	Make sure the function object is valid
			var isValid = (func.label && func.run);
			if (isValid){

				//	Look for existing settings
				var currentSelection = $$get(item, 'jaql.' + propName, null);

				//	Create the base menu item
				var subItem = {
					caption: func.label,
					checked: currentSelection === func.label,
					closing: false,
					disabled: false,
					size: "xl",
					type: "check",
					widget : widget,
					functionObj: func,
					item: item,
					panelItems: panelItems,
					execute: metadataItemClicked
				}

				//	Add header item
				options.items.push(subItem);
			}
		})
		
		//	Add options to the menu
		args.settings.items.push(separator);
		args.settings.items.push(options);	
    }

	//////////////////////////////////
	//	Business Logic				//
	//////////////////////////////////

	//  Function to add dashboard filters to the query
	function addDashboardFilters(query){

	    //  Get the list of dashboard filters, flattening out any cascading filters
	    var dashboardFilters = [];
	    prism.$ngscope.dashboard.filters.$$items.forEach(function(filter){

	        if (!filter.disabled) {
	            //  Check to see if the filter is cascading (dependant)
	            if (filter.isCascading) {
	                //  Need to loop through each level, and push the filter
	                filter.levels.forEach(function(levelFilter){
	                    dashboardFilters.push(levelFilter)
	                })
	            } else {
	                //  Regular filter, just add the jaql
	                dashboardFilters.push(filter.jaql)
	            }
	        }
	    })

	    dashboardFilters.forEach(function(filter){

	        //  Evaluate this filter
	        var isDateDim = (filter.datatype == "datetime");

	        //  Look for any matching filters @ the widget level
	        var matchingItems = query.metadata.filter(function(item){ 
	            var isMatch = (item.jaql.table == filter.table) && (item.jaql.column == filter.column),
	                dateCheck = !isDateDim ? true : (item.jaql.level == filter.level);
	            return isMatch && dateCheck;
	        })

	        //  Is this metadata item part of the existing query?
	        if (matchingItems.length > 0){

	            //  Find the matching item, and check if its a filter
	            var matchItem = matchingItems[0],
	                isWidgetFilter = (matchItem.panel == "scope");

	            //  Only do something if the match is not a widget filter
	            var filterCriteria = isWidgetFilter ? null : filter.filter;
	            if (filterCriteria){
	                matchItem.jaql.filter = filterCriteria;
	            }
	        } else {

	            //  Totally separate filter, so add it to the query
	            query.metadata.push({
	                'jaql': filter,
	                'panel': "scope"
	            })
	        }
	    })

	    return query;
	}

	//	Function to create a new JAQL Query
	function createJaql(queryStats, datasource, item){

		//	Create a new query
		var newQuery = {
			'datasource': datasource,
			'metadata': []
		}

		//	Add the dimensions 
		queryStats.dimensions.forEach(function(item){
			newQuery.metadata.push(item);
		})

		//	Break the finance function's jaql into more dimensions and measures
		var subDimensions = [],
			subMeasures = [],
			contexts = item.jaql.formula.match(/\[.*?\]/g);

		//	Loop through each part of the formula
		contexts.forEach(function(id){

			//	Make sure the context exists
			if (item.jaql.context.hasOwnProperty(id)){

				//	Get the sub-jaql
				var context = item.jaql.context[id];
				if (context.agg || context.formula){
					subMeasures.push({
						jaql: context
					});
				} else {
					subDimensions.push({
						jaql: context
					});
				}
			}
		})

		//	Add the sub dimensions
		subDimensions.forEach(function(item){
			newQuery.metadata.push(item);
		})				

		//	Add the sub measures
		subMeasures.forEach(function(item){
			newQuery.metadata.push(item);
		})	

		//	Add the dashboard/widget filters 
		queryStats.filters.forEach(function(item){
			newQuery.metadata.push(item);
		})

		//  Add dashboard filters
        newQuery = addDashboardFilters(newQuery);

		return newQuery;
	}

	//	Function to update the resultset
	function updateResults(widget, result, newData, index){

		//	Handle different types
		if (widget.type == "pivot") {
			updatePivot(widget, result, newData, index);
		} else {
			updateStandard(widget, result, newData, index);
		}
	}

	//	Function to update the resultset of pivot's specifically
	function updatePivot(widget, result, newData, index){

		//	Get a reference to the HTML table in memory
		var table = $('<div>' + result.table + '</div>');

		//	Find the cells within the specific column
		var cells = $('td.p-value[fidx=' + index + ']', table);

		//	Loop through each cell
		cells.each(function(idx, value){

			//	Get a reference to the raw value
			var newValue = newData[idx];

			//	Assign this value to the cell
			$(this).text(newValue);
		})

		//	Save the table back to the result
		result.table = table.html();
	}

	//	Function to update the results of standard Sisense widgets
	function updateStandard(widget, result, newData, index){

	}

	//	Function to get the midpoint
	function checkForFunctions(widget,args) {

		//	Analyze the JAQL to be run
		var queryStats = prism.$jaql.analyze(args.query.metadata);

		//	Loop through each metadata item from the query
		queryStats.measures.forEach(function(item){

			//	Check to see if the property is set
			var functionName = $$get(item, 'jaql.' + propName, false);

			//	Figure out which finance function needs to be run
			var myFunctionProp = myFunctions.filter(function(func){
				return func.label === functionName;
			});
			var myFunction = $$get(myFunctionProp,'0.run', null);

			//	Figure out the index for this item
			var indexes = args.rawResult.fields.filter(function(resultItem){
				return resultItem.id == item.field.id;
			})
			var index = $$get(indexes, '0.index', -1);

			//	Do we have everything needed to run a function?
			if (functionName && myFunction && (index >= 0)){

				//	Create a new jaql query
				var newQuery = createJaql(queryStats,args.query.datasource, item);

				//	Define the API call's options
				var options = {
					'method': 'POST',
					'url': '/jaql/query',
					'contentType': 'application/json',
					'async': false,
					'data': JSON.stringify(newQuery),
				}

				//	Run the API call for this query
				$.ajax(options).done(function(response){

					//	Calculate the new result set for this column
					var newData = myFunction(response, queryStats);

					//	Update the HTML table from EC, to include these new values
					updateResults(widget, args.rawResult, newData, index);
				})
			}
		})
	}

	//////////////////////////////////
	//	Event Handlers				//
	//////////////////////////////////
	
	// Registering dashboard/ widget creation in order to perform drilling
	prism.on("dashboardloaded", function (e, args) {
		args.dashboard.on("widgetinitialized", onWidgetInitialized);
	});

	// register widget events upon initialization
	function onWidgetInitialized(dashboard, args) {
		//	Hooking to ready/destroyed events
		addHandler(args.widget, 'destroyed', onWidgetDestroyed);
		//	Should we run this?
		var shouldInit = widgetIsSupported(args.widget.type);
		//	Add hook for rendering
		if (shouldInit) {
			addHandler(args.widget, 'queryend', checkForFunctions);
		}
	}

	// unregistering widget events
	function onWidgetDestroyed(widget, args) {
		widget.off("destroyed", onWidgetDestroyed);
	}

	//	Create Options for the quadrant analysis
	prism.on("beforemenu", addMenuItem);

}]);